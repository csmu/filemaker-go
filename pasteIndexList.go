package filemaker

// Object ...
type Object struct {
	ID int `xml:"id,attr" json:"id,omitempty"`
}

// PasteIndexList ...
type PasteIndexList struct {
	Membercount int       `xml:"membercount,attr" json:"memberCount,omitempty"`
	ObjectSlice []*Object `xml:"Object" json:"object,omitempty"`
}
