package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *CalcsForCustomFunctions) Process(path string) error {

	var err error
	var metaPath string
	var customFunctionCalc *CustomFunctionCalc

	if catalog != nil {
		if catalog.ObjectList != nil {
			for _, customFunctionCalc = range catalog.ObjectList.CustomFunctionCalc {
				if customFunctionCalc.CustomFunctionReference != nil {
					if customFunctionCalc.CustomFunctionReference.Name != "" {
						metaPath = fmt.Sprintf("%s (%d).json", customFunctionCalc.CustomFunctionReference.Name, customFunctionCalc.CustomFunctionReference.ID)
						saveJSONByteSliceToPath(customFunctionCalc, path, metaPath)
					}
				}
			}
		}
	}
	return err
}

// CalcsForCustomFunctions ...
type CalcsForCustomFunctions struct {
	Membercount int                                `xml:"membercount,attr" json:"memberCount"`
	ObjectList  *ObjectListCalcsForCustomFunctions `xml:"ObjectList" json:"objectList"`
}

// ObjectListCalcsForCustomFunctions ...
type ObjectListCalcsForCustomFunctions struct {
	Membercount        int                   `xml:"membercount,attr" json:"memberCount"`
	CustomFunctionCalc []*CustomFunctionCalc `xml:"CustomFunctionCalc" json:"customFunctionCalc"`
}
