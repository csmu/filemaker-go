package filemaker

import (
	"fmt"
)

// CustomFunction ...
type CustomFunction struct {
	ID         int                       `xml:"id,attr" json:"id,omitempty"`
	Name       string                    `xml:"name,attr" json:"name,omitempty"`
	Access     string                    `xml:"access,attr" json:"access,omitempty"`
	Display    string                    `xml:"Display" json:"display,omitempty"`
	ObjectList *ObjectListCustomFunction `xml:"ObjectList" json:"objectList,omitempty"`
}

// CustomFunctionCalc ...
type CustomFunctionCalc struct {
	CustomFunctionReference *CustomFunctionReference `xml:"CustomFunctionReference" json:"customFunctionReference,omitempty"`
	Calculation             *Calculation             `xml:"Calculation" json:"calculation,omitempty"`
}

// CustomFunctionsCatalog ...
type CustomFunctionsCatalog struct {
	Membercount int                               `xml:"membercount,attr" json:"memberCount,omitempty"`
	ObjectList  *ObjectListCustomFunctionsCatalog `xml:"ObjectList" json:"objectList,omitempty"`
}

// CustomFunctionReference ... A reference to a custom function
type CustomFunctionReference struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string ` xml:"name,attr" json:"name"`
}

// ObjectListCustomFunctionsCatalog ...
type ObjectListCustomFunctionsCatalog struct {
	Membercount         int               `xml:"membercount,attr" json:"memberCount,omitempty"`
	CustomFunctionSlice []*CustomFunction `xml:"CustomFunction" json:"customFunction,omitempty"`
}

// ObjectListCustomFunction ...
type ObjectListCustomFunction struct {
	Membercount int                                `xml:"membercount,attr" json:"memberCount,omitempty"`
	Parameter   []*ParameterCustomFunctionsCatalog `xml:"Parameter" json:"parameter,omitempty"`
}

// ParameterCustomFunctionsCatalog ...
type ParameterCustomFunctionsCatalog struct {
	Name string `xml:"name,attr" json:"name,omitempty"`
}

// Process ...
func (catalog *CustomFunctionsCatalog) Process(path string) error {
	var err error
	var metaPath string
	var model *CustomFunction
	if catalog != nil {
		if catalog.ObjectList != nil {
			for _, model = range catalog.ObjectList.CustomFunctionSlice {
				if model.Name != "" {
					metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
					saveJSONByteSliceToPath(model, path, metaPath)
				}
			}
		}
	}
	return err
}
