package filemaker

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"bd2l.com.au/csmu/utilities"
)

// GetTableDefinitionFromPath ...
// Returns a Table with a list of fields provided the contents of the directory
func GetTableDefinitionFromPath(path string) (Table, error) {

	var table Table
	var err error
	var fileInfoSlice []os.FileInfo
	var filePath string
	var fileInfo os.FileInfo
	var byteSlice []byte
	var field *Field
	var tableName string

	if utilities.PathIsDirectory(path) {

		tableName = filepath.Base(path)

		fileInfoSlice, err = ioutil.ReadDir(path)
		if err != nil {
			fmt.Println(err.Error())
		} else {

			table = Table{}
			table.FieldMap = make(map[string]Field)

			table.Name = tableName
			for _, fileInfo = range fileInfoSlice {
				filePath = filepath.Join(path, fileInfo.Name())
				//fmt.Println(filePath)
				byteSlice, err = ioutil.ReadFile(filePath)
				if err != nil {
					fmt.Println(err.Error())
				} else {
					// fmt.Println(string(byteSlice))
					err = json.Unmarshal(byteSlice, &field)
					if err != nil {
						fmt.Println(err.Error())
					} else {
						if strings.Contains(strings.ToLower(field.Name), "_id") {
							//fmt.Println(table.Name, field.Name, field.Validation.Unique)
						}
						table.FieldSlice = append(table.FieldSlice, *field)
						table.FieldMap[field.Name] = *field
					}
				}
			}
		}
	}
	return table, err
}
