package filemaker

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

type version struct {
	Prefix  *string   `json:"prefix"`
	Major   *int      `json:"major"`
	Minor   *int      `json:"minor"`
	Commits *int      `json:"commits"`
	Bug     time.Time `json:"bug"`
	Text    string    `json:"text"`
}

type fileInputOutput struct {
	Path   *string `json:"path"`
	Exists bool    `json:"exists"`
}

type inputOutput struct {
	Input  fileInputOutput `json:"input"`
	Output fileInputOutput `json:"output"`
}

func pathExists(path *string) bool {
	var result = false
	var err error
	_, err = os.Stat(*path)
	if err == nil {
		result = true
	}
	return result
}

func (version *version) mapPrevious(previous *version, incrementMajor *bool, incrementMinor *bool) {
	if version != nil {
		if previous != nil {

			var prefix = *version.Prefix
			var major = *version.Major
			var minor = *version.Minor
			var commits = *version.Commits
			var location *time.Location
			var dash = ""
			location, _ = time.LoadLocation("UTC")

			if prefix == "" {
				version.Prefix = previous.Prefix
			}
			if major == -1 {
				major = *previous.Major
				if *incrementMajor == true {
					major++
				}
				version.Major = &major
			}
			if minor == -1 {
				minor = *previous.Minor
				if *incrementMinor == true {
					minor++
				}
				version.Minor = &minor
			}
			if commits == -1 {
				commits = *previous.Commits
			}
			commits = commits + 1
			version.Commits = &commits
			if *version.Prefix != "" {
				dash = "-"
			}
			version.Text = fmt.Sprintf("%s%sv%d.%d.%d.%s", *version.Prefix, dash, *version.Major, *version.Minor, *version.Commits, version.Bug.In(location).Format("20060102-150405-UTC"))
		}
	}
}

func loadFrom(path string) (*version, error) {

	var err error
	var byteSlice []byte
	var file *os.File
	var result *version

	file, err = os.Open(path)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		byteSlice, err = ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			err = json.Unmarshal(byteSlice, &result)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	}

	return result, err
}
