package filemaker

import (
	"fmt"
	"sort"
	"strings"
)

// Table ...
type Table struct {
	Name       string           `json:"name"`
	FieldSlice []Field          `json:"fieldArray"`
	FieldMap   map[string]Field `json:"fieldMap"`
}

const POSTGRES = "postgres"

// Base ...
// returns the table as standard lower cased file name with a py extension
func (table *Table) Base() string {
	var result string
	result = fmt.Sprintf("%s.py", strings.ToLower(table.Name))
	return result
}

// AsDjangoClass  ...
func (table Table) AsDjangoClass(style string) []byte {

	var changesMap map[string]Field
	var field Field
	var fieldMap map[string]Field
	var filemakerIDsMap map[string]Field
	var foreignKeyMap map[string]Field
	var found bool
	var index string
	var modelName string
	var name string
	var ordered []string
	var result []byte
	var text string
	var validation Validation
	var oneToOne bool
	var comment string
	var integer bool
	var djangoPropertyName string
	var textFieldRequired bool
	var include bool
	var fieldName string
	var originalName string
	var verbose = false
	var primaryFound = false

	modelName = table.Name
	modelName = strings.ReplaceAll(modelName, "_", " ")
	modelName = strings.Title(strings.ToLower(modelName))
	modelName = strings.ReplaceAll(modelName, " ", "")
	if modelName == "SmsFile" {
		verbose = false
	}
	result = append(result, "# -*- coding: utf-8 -*-\n"...)
	result = append(result, "from __future__ import unicode_literals\n"...)
	result = append(result, "\n"...)
	result = append(result, "from django.db import models\n"...)
	result = append(result, "\n"...)
	result = append(result, "class "...)
	result = append(result, modelName...)
	result = append(result, "(models.Model):\n\n"...)

	fieldMap = make(map[string]Field)
	foreignKeyMap = make(map[string]Field)
	filemakerIDsMap = make(map[string]Field)
	changesMap = make(map[string]Field)

	for _, field = range table.FieldSlice {
		field := field
		fieldName = field.Name

		if verbose {
			fmt.Printf("verbose: %s %s %s %s\n", modelName, fieldName, field.FieldType, field.DataType)
		}

		if fieldName == "fk_serial" || fieldName == "fk_id" {
			/* ignore this total unstandard field name */
		} else {
			if strings.HasPrefix(fieldName, "pk_") && !primaryFound && (strings.HasSuffix(fieldName, "_serial") || strings.HasSuffix(fieldName, "_serial_number")) {
				fieldName = "id"
				primaryFound = true
			} else {
				if strings.HasSuffix(fieldName, "serial") {
					fieldName = strings.Replace(fieldName, "serial", "id", -1)
				}
				if strings.HasSuffix(fieldName, "serial_number") {
					fieldName = strings.Replace(fieldName, "serial_number", "id", -1)
				}
			}
		}

		fieldName = strings.ReplaceAll(fieldName, " ", "_")
		fieldName = strings.ReplaceAll(fieldName, "•", "")
		fieldName = strings.ReplaceAll(fieldName, "*", "")

		if verbose {
			fmt.Printf("verbose: %s %s %s %s\n", modelName, fieldName, field.FieldType, field.DataType)
		}

		if !primaryFound {
			if field.AutoEnter.SerialNumber != nil {
				primaryFound = true
				fieldName = "id"
			}

		}

		fieldMap[strings.ToLower(fieldName)] = field
	}

	// surrogate primary key
	if field, found = fieldMap["id"]; found {
		switch style {
		case POSTGRES:
			text = fmt.Sprintf("\tprimary = models.IntegerField(primary_key=True,help_text='%s',db_column='id') # field_id: %d\n", field.Name, field.ID)
		default:
			text = fmt.Sprintf("\tprimary = models.IntegerField(primary_key=True,help_text='id',db_column='%s') # field_id: %d\n", field.Name, field.ID)
		}
		result = append(result, text...)
		delete(fieldMap, "id")
	}

	// foreign surrogate keys
	for name, field = range fieldMap {
		field := field
		if strings.HasSuffix(strings.ToLower(name), "_id") {
			oneToOne = false
			switch name {
			case "record_id":
				break
			case "mod_id":
				break
			case "external_id":
				break
			default:
				if field.FieldType == "Normal" {
					if field.Storage.Global {
						// ignore
					} else {
						foreignKeyMap[strings.ToLower(name)] = field
						delete(fieldMap, name)
					}
				}
			}
		}
	}

	ordered = nil
	for name = range foreignKeyMap {
		ordered = append(ordered, name)
	}
	sort.Strings(ordered)
	for _, name = range ordered {
		field = foreignKeyMap[name]
		field := field

		validation = field.Validation
		if validation.Unique == true {
			oneToOne = true
		}

		comment = ""
		if field.Comment != nil {
			comment = fmt.Sprintf(" comment: %s", *field.Comment)
			comment = ""
		}

		name = strings.ToLower(name)
		name = strings.ReplaceAll(name, "fk_", "")
		name = strings.ReplaceAll(name, "_id", "")
		name = strings.ReplaceAll(name, "_serial", "")
		name = strings.ReplaceAll(name, "_", " ")
		name = strings.Title(strings.ToLower(name))
		djangoPropertyName = strings.ToLower(strings.ReplaceAll(name, " ", "_"))
		name = strings.ReplaceAll(name, " ", "")
		name = strings.ReplaceAll(name, "Default", "")
		name = strings.ReplaceAll(name, "Previous", "")
		name = strings.ReplaceAll(name, "Next", "")

		switch style {
		case POSTGRES:
			if oneToOne {
				text = fmt.Sprintf("\t%s = models.OneToOneField('%s',null=True,blank=True,help_text='%s',db_column='%s',db_index=True,related_name='%s_for_%s',on_delete=models.SET_NULL) # field_id: %d %s\n", djangoPropertyName, name, field.Name, strings.ReplaceAll(strings.ToLower(field.Name), "fk_", ""), strings.ToLower(modelName), djangoPropertyName, field.ID, comment)
			} else {
				text = fmt.Sprintf("\t%s = models.ForeignKey('%s',null=True,blank=True,help_text='%s',db_column='%s',db_index=True,related_name='%s_%s_list',on_delete=models.SET_NULL) # field_id: %d %s\n", djangoPropertyName, name, field.Name, strings.ReplaceAll(strings.ToLower(field.Name), "fk_", ""), strings.ToLower(modelName), djangoPropertyName, field.ID, comment)
			}
		default:
			if oneToOne {
				text = fmt.Sprintf("\t%s = models.OneToOneField('%s',null=True,blank=True,help_text='%s',db_column='%s',db_index=True,related_name='%s_for_%s',on_delete=models.SET_NULL) # field_id: %d %s\n", djangoPropertyName, name, strings.ReplaceAll(strings.ToLower(field.Name), "fk_", ""), field.Name, strings.ToLower(modelName), djangoPropertyName, field.ID, comment)
			} else {
				text = fmt.Sprintf("\t%s = models.ForeignKey('%s',null=True,blank=True,help_text='%s',db_column='%s',db_index=True,related_name='%s_%s_list',on_delete=models.SET_NULL) # field_id: %d %s\n", djangoPropertyName, name, strings.ReplaceAll(strings.ToLower(field.Name), "fk_", ""), field.Name, strings.ToLower(modelName), djangoPropertyName, field.ID, comment)
			}
		}

		result = append(result, text...)
	}

	// foreign universal unique identifiers
	for name, field = range fieldMap {
		originalName = name
		if strings.Contains(name, "_uuid") {
			name = strings.ReplaceAll(name, "fk_", "")
			name = strings.ReplaceAll(name, "_serial", "")
			name = strings.ReplaceAll(name, "_serial_number", "")
			name = strings.ReplaceAll(name, "_uuid", "")
			name = strings.ReplaceAll(name, "_", " ")
			name = strings.Title(strings.ToLower(name))
			djangoPropertyName = strings.ToLower(strings.ReplaceAll(name, " ", "_"))
			name = strings.ReplaceAll(name, " ", "")
			switch style {
			case POSTGRES:
				text = fmt.Sprintf("\t%s_uuid = models.UUIDField(null=True,blank=True,help_text='%s',db_column='%s',db_index=True) # field_id: %d\n", djangoPropertyName, field.Name, strings.ToLower(field.Name), field.ID)
			default:
				text = fmt.Sprintf("\t%s_uuid = models.CharField(max_length=128,null=True,blank=True,help_text='%s',db_column='%s',db_index=True) # field_id: %d\n", djangoPropertyName, field.Name, strings.ToLower(field.Name), field.ID)
			}

			result = append(result, text...)
			foreignKeyMap[strings.ToLower(name)] = field
			delete(fieldMap, originalName)
		}
	}

	// universal unique identifier
	for name, field = range fieldMap {
		switch style {
		case POSTGRES:
			if strings.ToLower(name) == "uuid" {
				text = fmt.Sprintf("\tuuid = models.UUIDField(null=True,blank=True,help_text='uuid',db_column='uuid',db_index=True,unique=True) # field_id: %d\n", field.ID)
				result = append(result, text...)
				delete(fieldMap, field.Name)
			}
			if strings.ToLower(name) == "uuid_included" {
				text = fmt.Sprintf("\tuuid_included = models.UUIDField(null=True,blank=True,help_text='uuid_included',db_column='uuid_included',db_index=True) # field_id: %d\n", field.ID)
				result = append(result, text...)
				delete(fieldMap, field.Name)
			}
		default:
			if strings.ToLower(name) == "uuid" {
				text = fmt.Sprintf("\tuuid = models.CharField(max_length=128,null=True,blank=True,help_text='uuid',db_column='uuid',db_index=True,unique=True) # field_id: %d\n", field.ID)
				result = append(result, text...)
				delete(fieldMap, field.Name)
			}
			if strings.ToLower(name) == "uuid_included" {
				text = fmt.Sprintf("\tuuid_included = models.CharField(max_length=128,null=True,blank=True,help_text='uuid_included',db_column='uuid_included',db_index=True) # field_id: %d\n", field.ID)
				result = append(result, text...)
				delete(fieldMap, field.Name)
			}
		}
	}

	result = append(result, "\n"...)

	for name, field = range fieldMap {
		if verbose {
			fmt.Printf("verbose: Change Fields: %s %s %s %s\n", modelName, fieldName, field.FieldType, field.DataType)
		}
		switch name {
		case "con":
			filemakerIDsMap[name] = field
			break
		case "record_id":
			filemakerIDsMap[name] = field
			break
		case "mod_id":
			filemakerIDsMap[name] = field
			break
		case "creation_account":
			changesMap[name] = field
			break
		case "creation_user":
			changesMap[name] = field
			break
		case "creation_date":
			changesMap[name] = field
			break
		case "Creation_Date":
			changesMap[name] = field
			break
		case "creation_timestamp":
			changesMap[name] = field
			break
		case "modification_account":
			changesMap[name] = field
			break
		case "modification_user":
			changesMap[name] = field
			break
		case "modification_date":
			changesMap[name] = field
			break
		case "Modification_Date":
			changesMap[name] = field
			break
		case "modification_timestamp":
			changesMap[name] = field
			break
		case "_namebuildfilter":
			// ignore
			break
		case "name_build_filter":
			// ignore
			break
		case "depot_id":
			// ignore
			break
		default:
			if strings.HasPrefix(name, "zz_") {
				/* ignore */
			} else {
				ordered = append(ordered, strings.ToLower(name))
			}
		}
	}

	sort.Strings(ordered)

	for _, name = range ordered {
		field = fieldMap[name]
		field := field
		delete(fieldMap, name)
		originalName = strings.ReplaceAll(name, " ", "_")
		if verbose {
			fmt.Printf("verbose: ordinary fields: %s %s %s %s\n", modelName, fieldName, field.FieldType, field.DataType)
		}
		name = strings.ToLower(field.Name)
		if field.Storage.AutoIndex {
			index = ",db_index=True"
		} else {
			index = ""
		}
		switch field.FieldType {
		case "Summary":
			break
		case "Calculated":
			break
		case "Normal":
			include = true
			if field.Storage.Global {
				// ignore
				include = false
			} else {

				comment = ""
				if field.Comment != nil {
					comment = fmt.Sprintf("comment: %s", *field.Comment)
					comment = ""
				}
				switch field.DataType {
				case "Number":
					integer = false
					if strings.ToLower(field.Name) == "depot_id" {
						include = false
					}
					if strings.Contains(field.Name, "flag_") {
						integer = true
					}
					if strings.Contains(field.Name, "view_in_") {
						integer = true
					}
					if strings.Contains(field.Name, "show_in_") {
						integer = true
					}
					if field.Name == "included" {
						integer = true
					}
					if field.Name == "deleted" {
						integer = true
					}
					if field.Name == "con" {
						integer = true
					}
					if field.Name == "constant" {
						integer = true
					}
					if include {
						switch style {
						case POSTGRES:
							if integer {
								text = fmt.Sprintf("\t%s = models.IntegerField(null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
							} else {
								text = fmt.Sprintf("\t%s = models.DecimalField(max_digits=20, decimal_places=2,null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
							}
						default:
							if integer {
								text = fmt.Sprintf("\t%s = models.IntegerField(null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
							} else {
								text = fmt.Sprintf("\t%s = models.DecimalField(max_digits=20, decimal_places=2,null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
							}
						}
						result = append(result, text...)
					}
					break
				case "Date":
					text = fmt.Sprintf("\t%s = models.DateField(null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
					result = append(result, text...)
					break
				case "Text":
					textFieldRequired = false
					include = true
					switch name {
					case "note":
						textFieldRequired = true
						break
					case "notes":
						textFieldRequired = true
						break
					case "xml_result":
						textFieldRequired = true
						break
					case "data_log":
						textFieldRequired = true
						include = false
						break
					case "change_log":
						textFieldRequired = true
						include = false
						break
					case "_namebuildfilter":
						textFieldRequired = true
						include = false
						break
					case "name_build_filter":
						textFieldRequired = true
						include = false
						break
					case "depot_id":
						include = false
						break
					default:
						textFieldRequired = false
						break
					}
					if include {
						if textFieldRequired {
							text = fmt.Sprintf("\t%s = models.TextField(null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
						} else {
							text = fmt.Sprintf("\t%s = models.CharField(max_length=512,null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
						}
						result = append(result, text...)
					} else {
						if verbose {
							fmt.Printf("verbose: excluded: %s %s %s %s\n", modelName, fieldName, field.FieldType, field.DataType)
						}
					}
				default:
					break
				}
			}
		default:
			break
		}
	}

	result = append(result, "\n"...)
	ordered = nil
	for name, field = range changesMap {
		ordered = append(ordered, name)
	}
	sort.Strings(ordered)
	for _, name = range ordered {
		originalName = strings.ReplaceAll(name, " ", "_")
		field = changesMap[name]
		switch field.DataType {
		case "Text":
			text = fmt.Sprintf("\t%s = models.CharField(max_length=512,null=True,blank=True,help_text='%s',db_column='%s') # field_id: %d\n", originalName, name, name, field.ID)
			result = append(result, text...)
			break
		case "Timestamp":
			text = fmt.Sprintf("\t%s = models.DateTimeField(null=True,blank=True,help_text='%s',db_column='%s') # field_id: %d\n", originalName, name, name, field.ID)
			result = append(result, text...)
			break
		case "Number":
			integer = false
			if strings.ToLower(field.Name) == "depot_id" {
				include = false
			}
			if strings.Contains(field.Name, "flag_") {
				integer = true
			}
			if strings.Contains(field.Name, "view_in_") {
				integer = true
			}
			if strings.Contains(field.Name, "show_in_") {
				integer = true
			}
			if field.Name == "included" {
				integer = true
			}
			if field.Name == "deleted" {
				integer = true
			}
			if field.Name == "con" {
				integer = true
			}
			if field.Name == "constant" {
				integer = true
			}
			if include {
				switch style {
				case POSTGRES:
					if integer {
						text = fmt.Sprintf("\t%s = models.IntegerField(null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
					} else {
						text = fmt.Sprintf("\t%s = models.DecimalField(max_digits=20, decimal_places=2,null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
					}
				default:
					if integer {
						text = fmt.Sprintf("\t%s = models.IntegerField(null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
					} else {
						text = fmt.Sprintf("\t%s = models.DecimalField(max_digits=20, decimal_places=2,null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
					}
				}
				result = append(result, text...)
			}
			break
		case "Date":
			text = fmt.Sprintf("\t%s = models.DateField(null=True,blank=True,help_text='%s',db_column='%s'%s) # field_id: %d%s\n", originalName, name, name, index, field.ID, comment)
			result = append(result, text...)
			break
		default:
			fmt.Printf("name: %s type: %s data: %s\n", field.Name, field.FieldType, field.DataType)
		}
	}
	changesMap = nil

	result = append(result, "\n"...)
	ordered = nil
	for name, field = range filemakerIDsMap {
		ordered = append(ordered, name)
	}
	sort.Strings(ordered)
	for _, name = range ordered {
		originalName = strings.ReplaceAll(name, " ", "_")
		field = filemakerIDsMap[name]
		text = fmt.Sprintf("\t%s = models.IntegerField(null=True,blank=True,help_text='%s',db_column='%s') # field_id: %d\n", originalName, name, name, field.ID)
		result = append(result, text...)
		delete(fieldMap, name)
	}

	result = append(result, "\n"...)
	result = append(result, "\tclass Meta:\n"...)
	switch style {
	case POSTGRES:
		result = append(result, "\t\tmanaged = True\n"...)
	default:
		result = append(result, "\t\tmanaged = False\n"...)
		text = fmt.Sprintf("\t\tdb_table = \"%s\"\n", table.Name)
		result = append(result, text...)
	}

	text = fmt.Sprintf("\t\tverbose_name = '%s'\n", modelName)
	result = append(result, text...)
	text = fmt.Sprintf("\t\tverbose_name_plural = '%sList'\n", modelName)
	result = append(result, text...)
	switch style {
	case POSTGRES:
		text = fmt.Sprintf("\n\tfilemaker_table_name = \"%s\"\n", table.Name)
		result = append(result, text...)
	default:
		// text = fmt.Sprintf("\n\tfilemaker_base_table_id = \"%d\"\n", table.ID)
		/* do nothing until we have the id of the table in the path */
		break
	}

	result = append(result, "\n"...)
	if verbose {
		fmt.Println(string(result))
	}

	return result
}
