package filemaker

// Field ...
type Field struct {
	Comment   *string `xml:"comment,attr" json:"comment,omitempty"`
	DataType  string  `xml:"datatype,attr" json:"dataType,omitempty"`
	FieldType string  `xml:"fieldtype,attr" json:"fieldType,omitempty"`
	ID        int     `xml:"id,attr" json:"id,omitempty"`
	Name      string  `xml:"name,attr" json:"name,omitempty"`

	AutoEnter  AutoEnter  `xml:"AutoEnter" json:"autoEnter,omitempty"`
	Storage    Storage    `xml:"Storage" json:"storage,omitempty"`
	Validation Validation `xml:"Validation" json:"validation,omitempty"`
}
