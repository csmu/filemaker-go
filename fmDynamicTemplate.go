package filemaker

import (
	"encoding/xml"
	"fmt"
	"path/filepath"
	"strings"

	"bd2l.com.au/csmu/utilities"
)

// Process ...
func (fmDynamicTemplate *FMDynamicTemplate) Process(path string) error {
	var err error
	var rootDirectoryPath = ""
	var directoryPath = ""
	if utilities.PathIsDirectory(path) {
		rootDirectoryPath = string(path)
	} else if utilities.PathIsFile(path) {
		rootDirectoryPath = filepath.Dir(path)
	}
	if rootDirectoryPath != "" {
		directoryPath = strings.Replace(fmDynamicTemplate.File, ".fmp12", "", -1)
		directoryPath = filepath.Join(rootDirectoryPath, directoryPath)
		if err == nil {

			err = createDirectoryIfItDoesNotExist(directoryPath, 0775)
			if err == nil {

				fmDynamicTemplate.Metadata.Process(directoryPath)
				fmDynamicTemplate.Structure.ProcessAccountsCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessBaseDirectoryCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessBaseTableCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessCalcsForCustomFunctions(directoryPath)
				fmDynamicTemplate.Structure.ProcessCustomFunctionsCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessCustomMenuCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessCustomMenuSetCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessExtendedPrivilegesCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessExternalDataSourceCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessFieldsForTables(directoryPath)
				fmDynamicTemplate.Structure.ProcessLayoutCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessPrivilegeSetsCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessRelationshipCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessScriptCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessStepsForScripts(directoryPath)
				fmDynamicTemplate.Structure.ProcessTableOccurrenceCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessThemeCatalog(directoryPath)
				fmDynamicTemplate.Structure.ProcessValueListCatalog(directoryPath)
			}
		}
	} else {
		err = fmt.Errorf("Can't determine directory path from %s", path)
	}
	return err
}

// Process ...
func (metadata *Metadata) Process(path string) error {
	var err error
	var meteDataPath string
	if metadata != nil {
		meteDataPath = ".metadata.json"
		err = saveJSONByteSliceToPath(metadata, path, meteDataPath)
	}
	return err
}

// ProcessAccountsCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessAccountsCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "AccountsCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.AccountsCatalog.Process(subdirectoryPath)
			structure.ModifyAction.AccountsCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessBaseDirectoryCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessBaseDirectoryCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "BaseDirectoryCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.BaseDirectoryCatalog.Process(subdirectoryPath)
			structure.ModifyAction.BaseDirectoryCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessBaseTableCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessBaseTableCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "BaseTableCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.BaseTableCatalog.Process(subdirectoryPath)
			structure.ModifyAction.BaseTableCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessCalcsForCustomFunctions ...
func (structure *StructureFMDynamicTemplate) ProcessCalcsForCustomFunctions(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "CalcsForCustomFunctions")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.CalcsForCustomFunctions.Process(subdirectoryPath)
			structure.ModifyAction.CalcsForCustomFunctions.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessCustomFunctionsCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessCustomFunctionsCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "CustomFunctionsCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.CustomFunctionsCatalog.Process(subdirectoryPath)
			structure.ModifyAction.CustomFunctionsCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessCustomMenuCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessCustomMenuCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "CustomMenuCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.CustomMenuCatalog.Process(subdirectoryPath)
			structure.ModifyAction.CustomMenuCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessCustomMenuSetCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessCustomMenuSetCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "CustomMenuSetCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.CustomMenuSetCatalog.Process(subdirectoryPath)
			structure.ModifyAction.CustomMenuSetCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessExtendedPrivilegesCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessExtendedPrivilegesCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "ExtendedPrivilegesCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.ExtendedPrivilegesCatalog.Process(subdirectoryPath)
			structure.ModifyAction.ExtendedPrivilegesCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessExternalDataSourceCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessExternalDataSourceCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "ExternalDataSourceCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.ExternalDataSourceCatalog.Process(subdirectoryPath)
			structure.ModifyAction.ExternalDataSourceCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessFieldsForTables ...
func (structure *StructureFMDynamicTemplate) ProcessFieldsForTables(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "FieldsForTables")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.FieldsForTables.Process(subdirectoryPath)
			structure.ModifyAction.FieldsForTables.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessLayoutCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessLayoutCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "LayoutCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.LayoutCatalog.Process(subdirectoryPath)
			structure.ModifyAction.LayoutCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessPrivilegeSetsCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessPrivilegeSetsCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "PrivilegeSetsCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.PrivilegeSetsCatalog.Process(subdirectoryPath)
			structure.ModifyAction.PrivilegeSetsCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessRelationshipCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessRelationshipCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "RelationshipCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.RelationshipCatalog.Process(subdirectoryPath)
			structure.ModifyAction.RelationshipCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessScriptCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessScriptCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "ScriptCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.ScriptCatalog.Process(subdirectoryPath)
			structure.ModifyAction.ScriptCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessStepsForScripts ...
func (structure *StructureFMDynamicTemplate) ProcessStepsForScripts(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "StepsForScripts")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.StepsForScripts.Process(subdirectoryPath)
			structure.ModifyAction.StepsForScripts.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessTableOccurrenceCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessTableOccurrenceCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "TableOccurrenceCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.TableOccurrenceCatalog.Process(subdirectoryPath)
			structure.ModifyAction.TableOccurrenceCatalog.Process(subdirectoryPath)
		}
		subdirectoryPath = filepath.Join(rootPath, "TableOccurrenceNotes")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			if structure.AddAction.TableOccurrenceCatalog != nil {
				structure.AddAction.TableOccurrenceCatalog.TableOccurrenceNotes.Process(subdirectoryPath)
			}
			if structure.ModifyAction.TableOccurrenceCatalog != nil {
				structure.ModifyAction.TableOccurrenceCatalog.TableOccurrenceNotes.Process(subdirectoryPath)
			}
		}
	}

	return err

}

// ProcessThemeCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessThemeCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "ThemeCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.ThemeCatalog.Process(subdirectoryPath)
			structure.ModifyAction.ThemeCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// ProcessValueListCatalog ...
func (structure *StructureFMDynamicTemplate) ProcessValueListCatalog(rootPath string) error {

	var err error
	var subdirectoryPath string

	if structure != nil {
		subdirectoryPath = filepath.Join(rootPath, "ValueListCatalog")
		err = createDirectoryIfItDoesNotExist(subdirectoryPath, 0775)
		if err == nil {
			structure.AddAction.ValueListCatalog.Process(subdirectoryPath)
			structure.ModifyAction.ValueListCatalog.Process(subdirectoryPath)
		}
	}

	return err

}

// AddActionStructure ... What gets added
type AddActionStructure struct {
	Membercount               int                        `xml:"membercount,attr" json:"memberCount,omitempty"`
	BaseDirectoryCatalog      *BaseDirectoryCatalog      `xml:"BaseDirectoryCatalog" json:"baseDirectoryCatalog,omitempty"`
	ExternalDataSourceCatalog *ExternalDataSourceCatalog `xml:"ExternalDataSourceCatalog" json:"externalDataSourceCatalog,omitempty"`
	BaseTableCatalog          *BaseTableCatalog          `xml:"BaseTableCatalog" json:"baseTableCatalog,omitempty"`
	TableOccurrenceCatalog    *TableOccurrenceCatalog    `xml:"TableOccurrenceCatalog" json:"tableOccurrenceCatalog,omitempty"`
	CalcsForCustomFunctions   *CalcsForCustomFunctions   `xml:"CalcsForCustomFunctions" json:"calcsForCustomFunctions,omitempty"`
	CustomFunctionsCatalog    *CustomFunctionsCatalog    `xml:"CustomFunctionsCatalog" json:"customFunctionsCatalog,omitempty"`
	CustomMenuCatalog         *CustomMenuCatalog         `xml:"CustomMenuCatalog" json:"customMenuCatalog,omitempty"`
	CustomMenuSetCatalog      *CustomMenuSetCatalog      `xml:"CustomMenuSetCatalog" json:"customMenuSetCatalog,omitempty"`
	FieldsForTables           *FieldsForTables           `xml:"FieldsForTables" json:"fieldsForTables,omitempty"`
	ValueListCatalog          *ValueListCatalog          `xml:"ValueListCatalog" json:"valueListCatalog,omitempty"`
	RelationshipCatalog       *RelationshipCatalog       `xml:"RelationshipCatalog" json:"relationshipCatalog,omitempty"`
	ScriptCatalog             *ScriptCatalog             `xml:"ScriptCatalog" json:"scriptCatalog,omitempty"`
	ThemeCatalog              *ThemeCatalog              `xml:"ThemeCatalog" json:"themeCatalog,omitempty"`
	LayoutCatalog             *LayoutCatalog             `xml:"LayoutCatalog" json:"layoutCatalog,omitempty"`
	PrivilegeSetsCatalog      *PrivilegeSetsCatalog      `xml:"PrivilegeSetsCatalog" json:"privilegeSetsCatalog,omitempty"`
	ExtendedPrivilegesCatalog *ExtendedPrivilegesCatalog `xml:"ExtendedPrivilegesCatalog" json:"ExtendedPrivilegesCatalog,omitempty"`
	AccountsCatalog           *AccountsCatalog           `xml:"AccountsCatalog" json:"AccountsCatalog,omitempty"`
	StepsForScripts           *StepsForScripts           `xml:"StepsForScripts" json:"StepsForScripts,omitempty"`
	PasteIndexList            *PasteIndexList            `xml:"PasteIndexList" json:"pasteIndexList,omitempty"`
}

// AddActionMetadata ...
type AddActionMetadata struct {
	Membercount int `xml:"membercount,attr" json:"memberCount,omitempty"`

	Defaults       *Defaults                      `xml:"Defaults" json:"defaults,omitempty"`
	HideToolbars   *MetadataAddActionHideToolbars `xml:"HideToolbars" json:"hideToolbars,omitempty"`
	IconData       *IconData                      `xml:"IconData" json:"iconData,omitempty"`
	Login          *Login                         `xml:"Login" json:"login,omitempty"`
	Minimum        *MetadataAddActionMinimum      `xml:"Minimum" json:"minimum,omitempty"`
	ScriptTriggers *ScriptTriggers                `xml:"ScriptTriggers" json:"scriptTriggers,omitempty"`
}

// FMDynamicTemplate ... Filemaker Save as XML Root node
type FMDynamicTemplate struct {
	XMLName xml.Name `xml:"FMDynamicTemplate" json:"fmDynamicTemplate,omitempty"`

	File    string `xml:"File,attr" json:"file,omitempty"`
	Locale  string `xml:"locale,attr" json:"locale,omitempty"`
	Source  string `xml:"Source,attr" json:"source,omitempty"`
	UUID    string `xml:"UUID,attr" json:"uuid,omitempty"`
	Version string `xml:"version,attr" json:"version,omitempty"`

	Metadata  Metadata                   `xml:"Metadata" json:"metadata,omitempty"`
	Structure StructureFMDynamicTemplate `xml:"Structure" json:"structure,omitempty"`
}

// Metadata ... The meta data of the FileMaker file.
type Metadata struct {
	Membercount int                `xml:"membercount,attr" json:"memberCount"`
	AddAction   *AddActionMetadata `xml:"AddAction" json:"addAction,omitempty"`
}

// ModifyAction ... What gets modified
type ModifyAction struct {
	Membercount int `xml:"membercount,attr" json:"memberCount,omitempty"`

	AccountsCatalog           *AccountsCatalog           `xml:"AccountsCatalog" json:"AccountsCatalog,omitempty"`
	BaseDirectoryCatalog      *BaseDirectoryCatalog      `xml:"BaseDirectoryCatalog" json:"baseDirectoryCatalog,omitempty"`
	BaseTableCatalog          *BaseTableCatalog          `xml:"BaseTableCatalog" json:"baseTableCatalog,omitempty"`
	CalcsForCustomFunctions   *CalcsForCustomFunctions   `xml:"CalcsForCustomFunctions" json:"calcsForCustomFunctions,omitempty"`
	CustomFunctionsCatalog    *CustomFunctionsCatalog    `xml:"CustomFunctionsCatalog" json:"customFunctionsCatalog,omitempty"`
	CustomMenuCatalog         *CustomMenuCatalog         `xml:"CustomMenuCatalog" json:"customMenuCatalog,omitempty"`
	CustomMenuSetCatalog      *CustomMenuSetCatalog      `xml:"CustomMenuSetCatalog" json:"customMenuSetCatalog,omitempty"`
	ExtendedPrivilegesCatalog *ExtendedPrivilegesCatalog `xml:"ExtendedPrivilegesCatalog" json:"ExtendedPrivilegesCatalog,omitempty"`
	ExternalDataSourceCatalog *ExternalDataSourceCatalog `xml:"ExternalDataSourceCatalog" json:"externalDataSourceCatalog,omitempty"`
	FieldsForTables           *FieldsForTables           `xml:"FieldsForTables" json:"fieldsForTables,omitempty"`
	LayoutCatalog             *LayoutCatalog             `xml:"LayoutCatalog" json:"layoutCatalog,omitempty"`
	PasteIndexList            *PasteIndexList            `xml:"PasteIndexList" json:"pasteIndexList,omitempty"`
	PrivilegeSetsCatalog      *PrivilegeSetsCatalog      `xml:"PrivilegeSetsCatalog" json:"privilegeSetsCatalog,omitempty"`
	RelationshipCatalog       *RelationshipCatalog       `xml:"RelationshipCatalog" json:"relationshipCatalog,omitempty"`
	ScriptCatalog             *ScriptCatalog             `xml:"ScriptCatalog" json:"scriptCatalog,omitempty"`
	StepsForScripts           *StepsForScripts           `xml:"StepsForScripts" json:"StepsForScripts,omitempty"`
	TableOccurrenceCatalog    *TableOccurrenceCatalog    `xml:"TableOccurrenceCatalog" json:"tableOccurrenceCatalog,omitempty"`
	ThemeCatalog              *ThemeCatalog              `xml:"ThemeCatalog" json:"themeCatalog,omitempty"`
	ValueListCatalog          *ValueListCatalog          `xml:"ValueListCatalog" json:"valueListCatalog,omitempty"`
}

// StructureFMDynamicTemplate ... The structure if the FilemMaker file
type StructureFMDynamicTemplate struct {
	Membercount  int                `xml:"membercount,attr" json:"memberCount"`
	AddAction    AddActionStructure `xml:"AddAction" json:"addAction,omitempty"`
	ModifyAction ModifyAction       `xml:"ModifyAction" json:"modifyAction,omitempty"`
}
