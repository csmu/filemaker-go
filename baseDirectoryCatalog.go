package filemaker

import (
	"fmt"
	"strings"
)

// BaseDirectory ... Where to find the main file
type BaseDirectory struct {
	ID         int    `xml:"id,attr" json:"id,omitempty"`
	Name       string `xml:"name,attr" json:"name"`
	RelativeTo bool   `xml:"relativeTo,attr" json:"relativeTo"`
}

// BaseDirectoryCatalog ... Path for this file
type BaseDirectoryCatalog struct {
	BaseDirectorySlice []*BaseDirectory `xml:"BaseDirectory" json:"baseDirectory,omitempty"`
	Generate           bool             `xml:"generate,attr" json:"generate,omitempty"`
	Membercount        int              `xml:"membercount,attr" json:"memberCount,omitempty"`
	Temporary          bool             `xml:"temporary,attr" json:"temporary,omitempty"`
}

// Process ...
func (catalog *BaseDirectoryCatalog) Process(path string) error {
	var err error
	var metaPath string
	var name string
	var model *BaseDirectory
	if catalog != nil {
		for _, model = range catalog.BaseDirectorySlice {
			if model.Name != "" {
				name = strings.Replace(model.Name, "/", "", -1)
				metaPath = fmt.Sprintf("%s.json", name)
				saveJSONByteSliceToPath(model, path, metaPath)
			}
		}
	}
	return err
}
