package filemaker

// Calculation ...
type Calculation struct {
	Text string `xml:"Text" json:"text,omitempty"`

	TableOccurrenceReference *TableOccurrenceReference `xml:"TableOccurrenceReference" json:"tableOccurrenceReference,omitempty"`
}
