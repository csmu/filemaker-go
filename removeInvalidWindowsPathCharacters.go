package filemaker

import "regexp"

// RemoveInvalidWindowsPathCharacters ...
// We had an issue where Windows refused to create files
func RemoveInvalidWindowsPathCharacters(path string) string {
	var re = regexp.MustCompile(`[\\~%&*/:<>?=|\"\n\r]`)
	var result string
	result = re.ReplaceAllString(path, "-")
	return result
}
