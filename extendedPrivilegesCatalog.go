package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *ExtendedPrivilegesCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *ExtendedPrivilege
	if catalog != nil {
		if catalog.ObjectList != nil {
			for _, model = range catalog.ObjectList.ExtendedPrivilegeSlice {
				if model.Name != "" {
					metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
					saveJSONByteSliceToPath(model, rootPath, metaPath)
				}
			}
		}

	}
	return err
}

// ExtendedPrivilege ...
type ExtendedPrivilege struct {
	Description string `xml:"Description" json:"description"`
	ID          int    `xml:"id,attr" json:"id"`
	Name        string `xml:"name,attr" json:"name"`

	ObjectList *ObjectListPrivilegeSetReferenceExtendedPrivilege `xml:"ObjectList" json:"objectList"`
}

// ExtendedPrivilegesCatalog ...
type ExtendedPrivilegesCatalog struct {
	Membercount int                           `xml:"membercount,attr" json:"memberCount"`
	ObjectList  *ObjectListExtendedPrivileges `xml:"ObjectList" json:"objectList"`
}

// ObjectListExtendedPrivileges ...
type ObjectListExtendedPrivileges struct {
	Membercount            int                  `xml:"membercount,attr" json:"memberCount"`
	ExtendedPrivilegeSlice []*ExtendedPrivilege `xml:"ExtendedPrivilege" json:"extendedPrivilege"`
}

// ObjectListPrivilegeSetReferenceExtendedPrivilege ...
type ObjectListPrivilegeSetReferenceExtendedPrivilege struct {
	Membercount           int                      `xml:"membercount,attr" json:"memberCount"`
	PrivilegeSetReference []*PrivilegeSetReference `xml:"PrivilegeSetReference" json:"privilegeSetReference"`
}
