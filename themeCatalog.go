package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *ThemeCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *Theme
	if catalog != nil {
		for _, model = range catalog.ThemeSlice {
			if model.Name != "" {
				metaPath = fmt.Sprintf("%s %s (%d).json", model.Display, model.Name, model.ID)
				saveJSONByteSliceToPath(model, rootPath, metaPath)
			}
		}
	}
	return err
}

// Image ...
type Image struct {
	Type   string `xml:"type,attr" json:"type"`
	Base64 string `xml:",chardata" json:"base64"`
}

// Theme ...
type Theme struct {
	Display  string `xml:"Display,attr" json:"display"`
	Group    string `xml:"Group,attr" json:"group"`
	Version  int    `xml:"version,attr" json:"version"`
	Locale   string `xml:"locale,attr" json:"locale"`
	Platform int    `xml:"platform,attr" json:"platform"`
	Name     string `xml:"name,attr" json:"name"`
	ID       int    `xml:"id,attr" json:"id"`
	Image    *Image `xml:"Image" json:"image"`
	CSS      string `xml:"CSS" json:"css"`
}

// ThemeCatalog ...
type ThemeCatalog struct {
	Membercount int      `xml:"membercount,attr" json:"memberCount"`
	ThemeSlice  []*Theme `xml:"Theme" json:"theme"`
}
