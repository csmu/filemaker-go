package filemaker

// ExternalDataSourceFile ...
type ExternalDataSourceFile struct {
	UniversalPathList string `xml:"UniversalPathList" json:"universalPathList"`
}
