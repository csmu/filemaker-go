package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *AccountsCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *Account
	if catalog != nil {
		if catalog.ObjectList != nil {
			for _, model = range catalog.ObjectList.AccountSlice {
				if model.Authentication != nil {
					metaPath = fmt.Sprintf("%s (%d).json", model.Authentication.AccountName, model.ID)
					saveJSONByteSliceToPath(model, rootPath, metaPath)
				}
			}
		}
	}
	return err
}

// Account ...
type Account struct {
	ID                    int                      `xml:"id,attr" json:"id"`
	Kind                  int                      `xml:"kind,attr" json:"kind"`
	Type                  string                   `xml:"type,attr" json:"type"`
	Enable                bool                     `xml:"enable,attr" json:"enable"`
	Description           string                   `xml:"Description" json:"description"`
	Authentication        *Authentication          `xml:"Authentication" json:"authentication"`
	PrivilegeSetReference []*PrivilegeSetReference `xml:"PrivilegeSetReference" json:"privilegeSetReference"`
}

// AccountsCatalog ...
type AccountsCatalog struct {
	Membercount int                 `xml:"membercount,attr" json:"memberCount"`
	Options     int                 `xml:"Options" json:"Options"`
	ObjectList  *ObjectListAccounts `xml:"ObjectList" json:"objectList"`
}

// Authentication ...
type Authentication struct {
	AccountName string `xml:"AccountName" json:"accountName"`
}

// ObjectListAccounts ...
type ObjectListAccounts struct {
	AccountSlice []*Account `xml:"Account" json:"Account"`
}
