package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *CustomMenuCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *CustomMenu
	if catalog != nil {
		if catalog.PasteIndexList != nil {
			metaPath = fmt.Sprintf(".pasteIndexList.json")
			saveJSONByteSliceToPath(catalog.PasteIndexList, rootPath, metaPath)
		}
		if catalog.CustomMenuSlice != nil {
			for _, model = range catalog.CustomMenuSlice {
				if model.Name != "" {
					metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
					saveJSONByteSliceToPath(model, rootPath, metaPath)
				}
			}
		}
	}
	return err
}

// Base ...
type Base struct {
	Name  string `xml:"name,attr" json:"name,omitempty"`
	Value int    `xml:"value,attr" json:"value,omitempty"`
}

// Chunk ...
type Chunk struct {
	Type  string `xml:"type,attr" json:"type,omitempty"`
	Value string `xml:",charattr" json:"value,omitempty"`
}

// ChunkList ...
type ChunkList struct {
	Chunk []*Chunk `xml:"Chunk" json:"chunk,omitempty"`
}

// Conditions ...
type Conditions struct {
	ChunkList *ChunkList `xml:"ChunkList" json:"chunkList,omitempty"`
	Install   *Install   `xml:"Install" json:"install,omitempty"`
	Options   *Options   `xml:"Options" json:"Options,omitempty"`
}

// CustomMenu ...
type CustomMenu struct {
	Comment string `xml:"Comment" json:"comment,omitempty"`
	ID      int    `xml:"id,attr" json:"id,omitempty"`
	Name    string `xml:"name,attr" json:"name,omitempty"`

	Base         *Base         `xml:"Base" json:"base,omitempty"`
	Conditions   *Conditions   `xml:"Conditions" json:"conditions,omitempty"`
	MenuItemList *MenuItemList `xml:"MenuItemList" json:"menuItemList,omitempty"`
	Options      *Options      `xml:"Options" json:"options,omitempty"`
}

// CustomMenuCatalog ...
type CustomMenuCatalog struct {
	Membercount int `xml:"membercount,attr" json:"memberCount,omitempty"`

	CustomMenuSlice []*CustomMenu   `xml:"CustomMenu" json:"customMenu,omitempty"`
	PasteIndexList  *PasteIndexList `xml:"PasteIndexList" json:"pasteIndexList,omitempty"`
}

// Install ...
type Install struct {
	Calculation string `xml:"Calculation" json:"calculation,omitempty"`
}

// MenuItem ...
type MenuItem struct {
	ID   int    `xml:"id,attr" json:"id,omitempty"`
	Name string `xml:"name,attr" json:"name,omitempty"`
}

// MenuItemList ...
type MenuItemList struct {
	Membercount   int         `xml:"membercount,attr" json:"memberCount,omitempty"`
	MenuItemSlice []*MenuItem `xml:"MenuItem" json:"menuItem,omitempty"`
}

// Options ...
type Options struct {
	BrowseMode  bool `xml:"browseMode,attr" json:"browseMode"`
	FindMode    bool `xml:"findMode,attr" json:"findMode"`
	PreviewMode bool `xml:"previewMode,attr" json:"previewMode"`
}
