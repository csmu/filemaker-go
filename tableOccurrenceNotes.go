package filemaker

import (
	"fmt"
)

// ObjectListTableOccurrenceNotes ...
type ObjectListTableOccurrenceNotes struct {
	Membercount       int             `xml:"membercount,attr" json:"memberCount,omitempty"`
	LayoutObjectSlice []*LayoutObject `xml:"LayoutObject" json:"LayoutObject,omitempty"`
}

// TableOccurrenceNotes ...
type TableOccurrenceNotes struct {
	ObjectList *ObjectListTableOccurrenceNotes `xml:"ObjectList" json:"ObjectList,omitempty"`
}

// Process ...
func (catalog *TableOccurrenceNotes) Process(path string) error {
	var err error
	var metaPath string
	var model *LayoutObject
	var name string
	if catalog != nil {
		if catalog.ObjectList != nil {
			if catalog.ObjectList.LayoutObjectSlice != nil {
				for _, model = range catalog.ObjectList.LayoutObjectSlice {
					if model.ID > 0 {
						name = ""
						if model.Text != nil {
							if model.Text.StyledText != nil {
								name = fmt.Sprintf("%s ", model.Text.StyledText.Data)
							}
						}
						metaPath = fmt.Sprintf("%s(%d).json", name, model.ID)
						saveJSONByteSliceToPath(model, path, metaPath)
					}
				}
			}
		}
	}
	return err
}
