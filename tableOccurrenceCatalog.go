package filemaker

import (
	"fmt"
)

// TableOccurrence ... An object in the FileMaker relationship graph
type TableOccurrence struct {
	ID                 int                           `xml:"id,attr" json:"id"`
	ViewState          string                        `xml:"viewState,attr" json:"viewState,omitempty"`
	Height             int                           `xml:"height,attr" json:"height,omitempty"`
	Name               string                        `xml:"name,attr" json:"name,omitempty"`
	Type               string                        `xml:"type,attr" json:"type,omitempty"`
	BaseTableReference *BaseTableReference           `xml:"BaseTableReference" json:"baseTableReference,omitempty"`
	FileReference      *FileReferenceTableOccurrence `xml:"FileReference" json:"fileReference,omitempty"`
	CoordRect          *CoordRect                    `xml:"CoordRect" json:"coordRect,omitempty"`
	Color              *Color                        `xml:"Color" json:"color,omitempty"`
}

// TableOccurrenceCatalog ...
type TableOccurrenceCatalog struct {
	Membercount          int                   `xml:"membercount,attr" json:"memberCount,omitempty"`
	TableOccurrenceNotes *TableOccurrenceNotes `xml:"TableOccurrenceNotes" json:"tableOccurrenceNotes,omitempty"`
	TableOccurrenceSlice []*TableOccurrence    `xml:"TableOccurrence" json:"tableOccurrence,omitempty"`
}

// TableOccurrenceReference ... A reference to a script
type TableOccurrenceReference struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string ` xml:"name,attr" json:"name"`
}

// Process ...
func (catalog *TableOccurrenceCatalog) Process(path string) error {
	var err error
	var metaPath string
	var model *TableOccurrence
	if catalog != nil {
		for _, model = range catalog.TableOccurrenceSlice {
			if model.Name != "" {
				metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
				saveJSONByteSliceToPath(model, path, metaPath)
			}
		}
	}
	return err
}
