package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *CustomMenuSetCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *CustomMenuSet
	if catalog != nil {
		if catalog.PasteIndexList != nil {
			metaPath = fmt.Sprintf(".pasteIndexList.json")
			saveJSONByteSliceToPath(catalog.PasteIndexList, rootPath, metaPath)
		}
		if catalog.ObjectList != nil {
			if catalog.ObjectList.CustomMenuSetSlice != nil {
				for _, model = range catalog.ObjectList.CustomMenuSetSlice {
					if model.Name != "" {
						metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
						saveJSONByteSliceToPath(model, rootPath, metaPath)
					}
				}
			}
		}
	}
	return err
}

// CustomMenuList ...
type CustomMenuList struct {
	CustomMenuReference []*CustomMenuReference `xml:"CustomMenuReference" json:"customMenuReference,omitempty"`
}

// CustomMenuSet ...
type CustomMenuSet struct {
	Comment        string          `xml:"comment,attr" json:"comment,omitempty"`
	ID             int             `xml:"id,attr" json:"id,omitempty"`
	Name           string          `xml:"name,attr" json:"name,omitempty"`
	CustomMenuList *CustomMenuList `xml:"CustomMenuList" json:"customMenuList,omitempty"`
}

// CustomMenuSetCatalog ...
type CustomMenuSetCatalog struct {
	Membercount int `xml:"membercount,attr" json:"memberCount,omitempty"`

	CustomMenuSetReference *CustomMenuSetReference  `xml:"CustomMenuSetReference" json:"customMenuSetReference,omitempty"`
	PasteIndexList         *PasteIndexList          `xml:"PasteIndexList" json:"pasteIndexList,omitempty"`
	ObjectList             *ObjectListCustomMenuSet `xml:"ObjectList" json:"objectList,omitempty"`
}

// CustomMenuReference ...
type CustomMenuReference struct {
	ID   int    `xml:"id,attr" json:"id,omitempty"`
	Name string `xml:"name,attr" json:"name,omitempty"`
}

// CustomMenuSetReference ...
type CustomMenuSetReference struct {
	ID   int    `xml:"id,attr" json:"id,omitempty"`
	Name string `xml:"name,attr" json:"name,omitempty"`
}

// ObjectListCustomMenuSet ...
type ObjectListCustomMenuSet struct {
	Membercount        int              `xml:"membercount,attr" json:"memberCount,omitempty"`
	CustomMenuSetSlice []*CustomMenuSet `xml:"CustomMenuSet" json:"customMenuSet,omitempty"`
}
