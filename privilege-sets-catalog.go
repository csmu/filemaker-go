package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *PrivilegeSetsCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *PrivilegeSet
	if catalog != nil {
		if catalog.ObjectList != nil {
			for _, model = range catalog.ObjectList.PrivilegeSetSlice {
				if model.Name != "" {
					metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
					saveJSONByteSliceToPath(model, rootPath, metaPath)
				}
			}
		}

	}
	return err
}

// AccessPrivilegeSet ...
type AccessPrivilegeSet struct {
	Default    *bool                         `xml:"default,attr" json:"default"`
	Records    *RecordsAccessPrivilegeSet    `xml:"Records" json:"records"`
	Layouts    *LayoutsAccessPrivilegeSet    `xml:"Layouts" json:"layouts"`
	ValueLists *ValueListsAccessPrivilegeSet `xml:"ValueLists" json:"valueLists"`
	Scripts    *ScriptsAccessPrivilegeSet    `xml:"Scripts" json:"scripts"`
	Other      *OtherPrivilegeSet            `xml:"Other" json:"other"`
}

// LayoutsAccessPrivilegeSet ...
type LayoutsAccessPrivilegeSet struct {
	Create    bool   `xml:"Create,attr" json:"create"`
	Edit      bool   `xml:"Edit,attr" json:"edit"`
	Delete    bool   `xml:"Delete,attr" json:"delete"`
	ViewState string `xml:"viewState,attr" json:"viewState"`
}

// ObjectListPrivilegeSet ...
type ObjectListPrivilegeSet struct {
	PrivilegeSetSlice []*PrivilegeSet `xml:"PrivilegeSet" json:"privilegeSet"`
}

// OtherPrivilegeSet ...
type OtherPrivilegeSet struct {
	Value          int    `xml:"value,attr" json:"value"`
	Print          bool   `xml:"Print,attr" json:"print"`
	Export         bool   `xml:"Export,attr" json:"export"`
	ManageExtPrivs bool   `xml:"manageExtPrivs,attr" json:"manageExtPrivs"`
	AllowOverride  bool   `xml:"allowOverride,attr" json:"allowOverride"`
	DisconnectIdle bool   `xml:"disconnectIdle,attr" json:"disconnectIdle"`
	Commands       string `xml:"commands,attr" json:"commands"`

	Password *PasswordPrivilegeSet `xml:"Password" json:"password"`
}

// PasswordPrivilegeSet ...
type PasswordPrivilegeSet struct {
	ProhibitModification *bool `xml:"prohibitModification,attr" json:"prohibitModification"`
}

// PrivilegeSet ...
type PrivilegeSet struct {
	ID          int                 `xml:"id,attr" json:"id"`
	Name        string              `xml:"name,attr" json:"name"`
	Description string              `xml:"Description" json:"description"`
	Access      *AccessPrivilegeSet `xml:"access" json:"access"`
}

// PrivilegeSetsCatalog ...
type PrivilegeSetsCatalog struct {
	Membercount int                     `xml:"membercount,attr" json:"memberCount"`
	ObjectList  *ObjectListPrivilegeSet `xml:"ObjectList" json:"objectList"`
}

// PrivilegeSetReference ...
type PrivilegeSetReference struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string `xml:"name,attr" json:"name"`
}

// RecordsAccessPrivilegeSet ...
type RecordsAccessPrivilegeSet struct {
	Create    bool   `xml:"Create,attr" json:"create"`
	Edit      bool   `xml:"Edit,attr" json:"edit"`
	Delete    bool   `xml:"Delete,attr" json:"delete"`
	ViewState string `xml:"viewState,attr" json:"viewState"`
}

// ScriptsAccessPrivilegeSet ...
type ScriptsAccessPrivilegeSet struct {
	Create    bool   `xml:"Create,attr" json:"create"`
	Edit      bool   `xml:"Edit,attr" json:"edit"`
	Delete    bool   `xml:"Delete,attr" json:"delete"`
	ViewState string `xml:"viewState,attr" json:"viewState"`
}

// ValueListsAccessPrivilegeSet ...
type ValueListsAccessPrivilegeSet struct {
	Create    bool   `xml:"Create,attr" json:"create"`
	Edit      bool   `xml:"Edit,attr" json:"edit"`
	Delete    bool   `xml:"Delete,attr" json:"delete"`
	ViewState string `xml:"viewState,attr" json:"viewState"`
}
