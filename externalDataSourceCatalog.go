package filemaker

import (
	"fmt"
)

// ExternalDataSource ... Where to find the main file
type ExternalDataSource struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string `xml:"name,attr" json:"name"`
	Type string `xml:"type,attr" json:"type"`

	File *ExternalDataSourceFile `xml:"File" json:"file"`
}

// ExternalDataSourceCatalog ... Path for this file
type ExternalDataSourceCatalog struct {
	Membercount             int                   `xml:"membercount,attr" json:"memberCount"`
	ExternalDataSourceSlice []*ExternalDataSource `xml:"ExternalDataSource" json:"externalDataSource,omitempty"`
}

// Process ...
func (catalog *ExternalDataSourceCatalog) Process(path string) error {
	var err error
	var metaPath string
	var model *ExternalDataSource
	if catalog != nil {
		for _, model = range catalog.ExternalDataSourceSlice {
			if model.Name != "" {
				metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
				saveJSONByteSliceToPath(model, path, metaPath)
			}
		}
	}
	return err
}
