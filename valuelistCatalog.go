package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *ValueListCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *ValueList
	if catalog != nil {
		for _, model = range catalog.ValueListSlice {
			if model.Name != "" {
				metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
				saveJSONByteSliceToPath(model, rootPath, metaPath)
			}
		}
	}
	return err
}

// CustomValues ...
type CustomValues struct {
	Text string `xml:"Text" json:"text,omitempty"`
}

// ValueList ...
type ValueList struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string `xml:"name,attr" json:"name"`

	CustomValues *CustomValues      `xml:"CustomValues" json:"customValues,omitempty"`
	External     *ExternalValueList `xml:"External" json:"external,omitempty"`
	Field        *FieldValueList    `xml:"Field" json:"field,omitempty"`
	Source       *SourceValueList   `xml:"Source" json:"source,omitempty"`
}

// ValueListCatalog ...
type ValueListCatalog struct {
	Membercount    int          `xml:"membercount,attr" json:"memberCount"`
	ValueListSlice []*ValueList `xml:"ValueList" json:"valueList"`
}

// ValueListReference ...
type ValueListReference struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string ` xml:"name,attr" json:"name"`
}
