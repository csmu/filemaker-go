package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *StepsForScripts) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *ScriptStepsForScripts
	if catalog != nil {
		if catalog.ScriptStepsForScriptsSlice != nil {
			for _, model = range catalog.ScriptStepsForScriptsSlice {
				if model.ScriptReference != nil {
					metaPath = fmt.Sprintf("%s (%d).json", model.ScriptReference.Name, model.ScriptReference.ID)
					saveJSONByteSliceToPath(model, rootPath, metaPath)
				}
			}
		}
	}
	return err
}

// AnimationParameterStepsForScripts ...
type AnimationParameterStepsForScripts struct {
	Name  string `xml:"name,attr" json:"name,omitempty"`
	Value int    `xml:"value,attr" json:"value,omitempty"`
}

// BooleanParameterStepsForScripts ...
type BooleanParameterStepsForScripts struct {
	Type  *string `xml:"type,attr" json:"type,omitempty"`
	ID    int     `xml:"id,attr" json:"id,omitempty"`
	Value bool    `xml:"value,attr" json:"value"`
}

// CalculationStepsForScripts ...
type CalculationStepsForScripts struct {
	Text string `xml:"Text" json:"text,omitempty"`
}

// CalculationParameterStepsForScripts ...
type CalculationParameterStepsForScripts struct {
	Datatype    int                         `xml:"value,attr" json:"value,omitempty"`
	Position    int                         `xml:"position,attr" json:"position,omitempty"`
	Calculation *CalculationStepsForScripts `xml:"Calculation" json:"calculation,omitempty"`
}

// CommentParameterStepsForScripts ...
type CommentParameterStepsForScripts struct {
	Value string `xml:"value,attr" json:"value,omitempty"`
}

// CustomMenuSetReferenceParameterStepsForScripts ...
type CustomMenuSetReferenceParameterStepsForScripts struct {
	ID      int                              `xml:"id,attr" json:"id,omitempty"`
	Name    string                           `xml:"name,attr" json:"name,omitempty"`
	Boolean *BooleanParameterStepsForScripts `xml:"Boolean" json:"boolean,omitempty"`
}

// LayoutReferenceContainerStepsForScripts ...
type LayoutReferenceContainerStepsForScripts struct {
	Value           int              `xml:"value,attr" json:"value,omitempty"`
	LayoutReference *LayoutReference `xml:"LayoutReference" json:"layoutReference,omitempty"`
}

// ListParameterStepsForScripts ...
type ListParameterStepsForScripts struct {
	Name            string           `xml:"name,attr" json:"name,omitempty"`
	ScriptReference *ScriptReference `xml:"ScriptReference" json:"scriptReference,omitempty"`
	Value           int              `xml:"value,attr" json:"value,omitempty"`
}

// ParameterStepsForScripts ...
type ParameterStepsForScripts struct {
	ValueAttribute           string                                          `xml:"value,attr" json:"valueAttribute,omitempty"`
	Type                     string                                          `xml:"type,attr" json:"type,omitempty"`
	LayoutReferenceContainer *LayoutReferenceContainerStepsForScripts        `xml:"LayoutReferenceContainer" json:"layoutReferenceContainer,omitempty"`
	Animation                *AnimationParameterStepsForScripts              `xml:"Animation" json:"animation,omitempty"`
	Calculation              *CalculationParameterStepsForScripts            `xml:"Calculation" json:"calculation,omitempty"`
	List                     *ListParameterStepsForScripts                   `xml:"List" json:"list,omitempty"`
	Boolean                  *BooleanParameterStepsForScripts                `xml:"Boolean" json:"boolean,omitempty"`
	Comment                  *CommentParameterStepsForScripts                `xml:"Comment" json:"comment,omitempty"`
	CustomMenuSetReference   *CustomMenuSetReferenceParameterStepsForScripts `xml:"CustomMenuSetReference" json:"customMenuSetReference,omitempty"`
	Value                    *ValueParameterStepsForScripts                  `xml:"value" json:"value,omitempty"`
	Name                     *NameParameterStepsForScripts                   `xml:"Name" json:"name,omitempty"`
	Repetition               *int                                            `xml:"Repetition" json:"repetition,omitempty"`
	FieldReference           *FieldReference                                 `xml:"FieldReference" json:"fieldReference,omitempty"`
}

// ParameterValuesStepsForScripts ...
type ParameterValuesStepsForScripts struct {
	Membercount int                         `xml:"membercount,attr" json:"memberCount"`
	Parameter   []*ParameterStepsForScripts `xml:"Parameter" json:"parameter,omitempty"`
}

// NameParameterStepsForScripts ...
type NameParameterStepsForScripts struct {
	Value string `xml:"value,attr" json:"value"`
}

// StepStepsForScripts ...
type StepStepsForScripts struct {
	ID              int                             `xml:"id,attr" json:"id,omitempty"`
	Name            string                          `xml:"name,attr" json:"name,omitempty"`
	Enable          bool                            `xml:"Enable,attr" json:"enable,omitempty"`
	Options         int                             `xml:"Options" json:"options,omitempty"`
	ParameterValues *ParameterValuesStepsForScripts `xml:"ParameterValues" json:"parameterValues,omitempty"`
}

// StepObjectListStepsForScripts ...
type StepObjectListStepsForScripts struct {
	Step []*StepStepsForScripts `xml:"Step" json:"step,omitempty"`
}

// ScriptStepsForScripts ...
type ScriptStepsForScripts struct {
	ScriptReference *ScriptReference               `xml:"ScriptReference" json:"scriptReference,omitempty"`
	ObjectList      *StepObjectListStepsForScripts `xml:"ObjectList" json:"objectList,omitempty"`
}

// StepsForScripts ...
type StepsForScripts struct {
	Membercount                int                      `xml:"membercount,attr" json:"memberCount"`
	ScriptStepsForScriptsSlice []*ScriptStepsForScripts `xml:"Script" json:"script,omitempty"`
}

// ValueParameterStepsForScripts ...
type ValueParameterStepsForScripts struct {
	Calculation *CalculationParameterStepsForScripts `xml:"Calculation" json:"calculation,omitempty"`
}
