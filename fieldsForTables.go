package filemaker

import (
	"fmt"
	"path/filepath"
)

// Process ...
func (catalog *FieldsForTables) Process(rootPath string) error {

	var err error
	var subDirectoryPath string
	var metaPath string
	var model *FieldCatalog
	var field *Field

	if catalog != nil {
		for _, model = range catalog.FieldCatalogSlice {
			if model.TableOccurrenceReference != nil {
				subDirectoryPath = filepath.Join(rootPath, model.TableOccurrenceReference.Name)
				err = createDirectoryIfItDoesNotExist(subDirectoryPath, 0775)
				if err == nil {
					if model.ObjectList != nil {
						for _, field = range model.ObjectList.FieldSlice {
							if field.Name != "" {
								metaPath = fmt.Sprintf("%s (%d).json", field.Name, field.ID)
								saveJSONByteSliceToPath(field, subDirectoryPath, metaPath)
							}
						}
					}
				}
			}
		}
	}
	return err
}

// AutoEnter ...
type AutoEnter struct {
	ProhibitModification bool   `xml:"prohibitModification,attr" json:"prohibitModification,omitempty"`
	Type                 string `xml:"type,attr" json:"type,omitempty"`

	Calculated   *Calculated   `xml:"Calculated" json:"calculated,omitempty"`
	SerialNumber *SerialNumber `xml:"SerialNumber" json:"serialNumber,omitempty"`
}

// FieldCatalog ...
type FieldCatalog struct {
	TableOccurrenceReference *TableOccurrenceReference `xml:"TableOccurrenceReference" json:"tableOccurrenceReference"`
	ObjectList               *ObjectListField          `xml:"ObjectList" json:"objectList"`
}

// FieldsForTables ...
type FieldsForTables struct {
	Membercount       int             `xml:"membercount,attr" json:"memberCount"`
	FieldCatalogSlice []*FieldCatalog `xml:"FieldCatalog" json:"fieldCatalog"`
}

// FieldReference ...
type FieldReference struct {
	ID                  int         `xml:"id,attr" json:"id"`
	Name                string      `xml:"name,attr" json:"name"`
	RepetitionAttribute int         `xml:"repetition,attr" json:"repetitionAttribute,omitempty"`
	Repetition          *Repetition `xml:"repetition" json:"repetition,omitempty"`
	TableOccurrence     string      `xml:"tableOccurrence,attr" json:"tableOccurrence"`
}

// ObjectListField ...
type ObjectListField struct {
	Membercount int      `xml:"membercount,attr" json:"memberCount"`
	FieldSlice  []*Field `xml:"Field" json:"field,omitempty"`
}

// Storage ... Where or not the field is global, has an index, and the number of repitions .. which should always be 1
type Storage struct {
	AutoIndex         bool               `xml:"autoIndex,attr" json:"autoIndex"`
	Index             string             `xml:"index" json:"index"`
	Global            bool               `xml:"global,attr" json:"global"`
	MaxRepetitions    int                `xml:"maxRepetitions,attr" json:"maxRepetitions"`
	LanguageReference *LanguageReference `xml:"LanguageReference" json:"languageReference"`
}
