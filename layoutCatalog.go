package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *LayoutCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *Layout
	if catalog != nil {
		for _, model = range catalog.LayoutSlice {
			if model.Name != "" {
				metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
				saveJSONByteSliceToPath(model, rootPath, metaPath)
			}
		}
	}
	return err
}

// Bounds ..
type Bounds struct {
	Top    int `xml:"top,attr" json:"top"`
	Left   int `xml:"left,attr" json:"left"`
	Bottom int `xml:"bottom,attr" json:"bottom"`
	Right  int `xml:"right,attr" json:"right"`
}

// CalculationLayout ...
type CalculationLayout struct {
	Text string `xml:"Text" json:"text"`
}

// DefinitionPart ...
type DefinitionPart struct {
	Type     string `xml:"type,attr" json:"type"`
	Kind     int    `xml:"kind,attr" json:"kind"`
	Size     int    `xml:"size,attr" json:"size"`
	Absolute int    `xml:"absolute,attr" json:"absolute"`
	Options  int    `xml:"Options,attr" json:"options"`
}

// DisplayLayout ...
type DisplayLayout struct {
	Style       int                `Style:"top,attr" json:"style"`
	Show        int                `xml:"show,attr" json:"show"`
	Placeholder *PlaceholderLayout `xml:"Placeholder" json:"placeholder"`
}

// ExtendedAttributes ...
type ExtendedAttributes struct {
	Formatting *FormattingLayout `xml:"Formatting" json:"formatting"`
}

// FieldLayout ...
type FieldLayout struct {
	FieldReference *FieldReference `xml:"FieldReference" json:"fieldReference"`
	Options        int             `xml:"Options" json:"options"`
	Display        *DisplayLayout  `xml:"Display" json:"display"`
	Usage          *UsageLayout    `xml:"Usage" json:"usage"`
}

// FormattingLayout ...
type FormattingLayout struct {
	Graphic *GraphicLayout `xml:"Graphic" json:"graphic"`
	Time    *TimeLayout    `xml:"Time" json:"time"`
	Numeric *Numeric       `xml:"Numeric" json:"numeric"`
}

// GraphicLayout ...
type GraphicLayout struct {
	Options int `xml:"Options" json:"options"`
}

// IconDataLayout ...
type IconDataLayout struct {
	Type int `xml:"type,attr" json:"type"`
	Size int `xml:"size,attr" json:"size"`
}

// Layout ...
type Layout struct {
	ID    int    `xml:"id,attr" json:"id"`
	Name  string `xml:"name,attr" json:"name"`
	Width int    `xml:"width,attr" json:"width"`

	LayoutThemeReference     *LayoutThemeReference     `xml:"LayoutThemeReference" json:"layoutThemeReference"`
	PartsList                *PartsList                `xml:"PartsList" json:"PartsList"`
	TableOccurrenceReference *TableOccurrenceReference `xml:"TableOccurrenceReference" json:"tableOccurrenceReference"`
}

// LayoutObject ...
type LayoutObject struct {
	ID      int    `xml:"id,attr" json:"id,omitempty"`
	Kind    int    `xml:"kind,attr" json:"kind,omitempty"`
	Name    string `xml:"name,attr" json:"name,omitempty"`
	Options int    `xml:"Options" json:"options,omitempty"`
	Type    string `xml:"type,attr" json:"type,omitempty"`

	Bounds             *Bounds              `xml:"Bounds" json:"bounds,omitempty"`
	ExtendedAttributes *ExtendedAttributes  `xml:"ExtendedAttributes" json:"extendedAttributes,omitempty"`
	Field              *FieldLayout         `xml:"Field" json:"field,omitempty"`
	LocalCSS           *LocalCSS            `xml:"LocalCSS" json:"localCSS,omitempty"`
	PopoverButton      *PopoverButtonLayout `xml:"PopoverButton" json:"popoverButton,omitempty"`
	Portal             *Portal              `xml:"Portal" json:"portal,omitempty"`
	Text               *Text                `xml:"Text" json:"text,omitempty"`
}

// LayoutReference ... A reference to a layout
type LayoutReference struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string ` xml:"name,attr" json:"name"`
}

// LabelLayout ...
type LabelLayout struct {
	Text *Text `xml:"Text" json:"text"`
}

// LayoutCatalog ...
type LayoutCatalog struct {
	Membercount int       `xml:"membercount,attr" json:"memberCount"`
	LayoutSlice []*Layout `xml:"Layout" json:"Layout"`
}

// LayoutThemeReference ...
type LayoutThemeReference struct {
	Base string `xml:"Base,attr" json:"base"`
	ID   int    `xml:"id,attr" json:"id"`
	Name string `xml:"name,attr" json:"name"`
}

// LocalCSS ...
type LocalCSS struct {
	Name string `xml:"name,attr" json:"name"`
	CSS  string `xml:",chardata" json:"css"`
}

// NegativeStyle ...
type NegativeStyle struct {
	Color *Color `xml:"Color" json:"Color"`
}

// Numeric ...
type Numeric struct {
	Options       int           `xml:"Options" json:"options"`
	Style         *StyleNumeric `xml:"Style" json:"style"`
	DecimalDigits int           `xml:"DecimalDigits" json:"decimalDigits"`
}

// ObjectListLayout ...
type ObjectListLayout struct {
	Membercount  int             `xml:"membercount,attr" json:"memberCount"`
	LayoutObject []*LayoutObject `xml:"LayoutObject" json:"layoutObject"`
}

// OptionsLayout ...
type OptionsLayout struct {
	Index int `xml:"index,attr" json:"index,omitempty"`
	Show  int `xml:"show,attr" json:"show,omitempty"`
	Value int `xml:",chardata" json:"value,omitempty"`
}

// PartLayout ...
type PartLayout struct {
	Type       string            `xml:"type,attr" json:"type"`
	Kind       int               `xml:"kind,attr" json:"kind"`
	Definition *DefinitionPart   `xml:"Definition" json:"definition"`
	ObjectList *ObjectListLayout `xml:"ObjectList" json:"objectList"`
}

// PartsList ...
type PartsList struct {
	Membercount int           `xml:"membercount,attr" json:"memberCount"`
	Part        []*PartLayout `xml:"Part" json:"part"`
}

// PlaceholderLayout ...
type PlaceholderLayout struct {
	FindMode    bool               `xml:"findMode,attr" json:"findMode"`
	Calculation *CalculationLayout `xml:"Calculation" json:"calculation"`
}

// PopoverButtonLayout ...
type PopoverButtonLayout struct {
	Options      int             `xml:"Options" json:"options"`
	Label        *LabelLayout    `xml:"Label" json:"label"`
	IconData     *IconDataLayout `xml:"IconData" json:"iconData"`
	PopoverPanel *PopoverPanel   `xml:"PopoverPanel" json:"popoverPanel"`
}

// PopoverPanel ...
type PopoverPanel struct {
	ID         int               `xml:"id,attr" json:"id"`
	Type       string            `xml:"type,attr" json:"type"`
	Name       string            `xml:"name,attr" json:"name"`
	Kind       int               `xml:"kind,attr" json:"kind"`
	Position   int               `xml:"position,attr" json:"position"`
	Options    int               `xml:"Options" json:"options"`
	Title      *TitleLayout      `xml:"Title" json:"Title"`
	ObjectList *ObjectListLayout `xml:"ObjectList" json:"objectList"`
}

// Portal ...
type Portal struct {
	Options                  *OptionsLayout            `xml:"Options" json:"options,omitempty"`
	ObjectList               *ObjectListLayout         `xml:"ObjectList" json:"objectList,omitempty"`
	SortSpecification        *SortSpecification        `xml:"SortSpecification" json:"sortSpecification,omitempty"`
	TableOccurrenceReference *TableOccurrenceReference `xml:"TableOccurrenceReference" json:"tableOccurrenceReference"`
}

// PrimaryFieldLayout ...
type PrimaryFieldLayout struct {
	FieldReference *FieldReference `xml:"FieldReference" json:"fieldReference"`
}

// Sort ...
type Sort struct {
	Type         string              `xml:"type,attr" json:"type"`
	PrimaryField *PrimaryFieldLayout `xml:"PrimaryFieldLayout" json:"primaryFieldLayout"`
}

// SortList ...
type SortList struct {
	Membercount int `xml:"membercount,attr" json:"memberCount"`
}

// SortSpecification ...
type SortSpecification struct {
	Value    bool      `xml:"value,attr" json:"value"`
	Maintain bool      `xml:"maintain,attr" json:"maintain"`
	SortList *SortList `xml:"SortList" json:"sortList"`
}

// StyledText ...
type StyledText struct {
	Data string `xml:"Data" json:"data"`
}

// StyleNumeric ...
type StyleNumeric struct {
	Negative *NegativeStyle `xml:"Negative" json:"negative"`
}

// Text ...
type Text struct {
	Options    int         `xml:"Options" json:"options"`
	StyledText *StyledText `xml:"StyledText" json:"styledText"`
}

// TitleLayout ...
type TitleLayout struct {
	Text string `xml:"Text" json:"Text"`
}

// TimeLayout ...
type TimeLayout struct {
	Options int `xml:"Options" json:"options"`
}

// UsageLayout ...
type UsageLayout struct {
	InputMode int `xml:"inputMode,attr" json:"inputMode"`
	Type      int `xml:"type,attr" json:"type"`
}
