package filemaker

import (
	"github.com/Jeffail/gabs"
)

func version() string {
	var result string
	var err error
	var gabsContainer *gabs.Container
	gabsContainer, err = gabs.ParseJSON([]byte(`
	{
    "prefix": "filemaker",
    "major": 1,
    "minor": 1,
    "Commits": 21,
    "bug": "2020-02-03T16:04:09.506421+10:30",
    "text": "filemaker-v1.1.21.20200203-053409-UTC"
}
	`))
	if err != nil {
		result = err.Error()
	} else {
		result = gabsContainer.StringIndent("", "    ")
	}
	return result
}
