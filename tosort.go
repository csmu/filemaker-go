package filemaker

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"bd2l.com.au/csmu/utilities"
	"github.com/joho/godotenv"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
)

// BinaryData ... list of streams to create a binary object
type BinaryData struct {
	LibraryReference *BinaryDataLibraryReference `xml:"LibraryReference" json:"libraryReference"`
	StreamList       []*BinaryDataStream
}

// BinaryDataLibraryReference ...
type BinaryDataLibraryReference struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string ` xml:"name,attr" json:"name"`
}

// BinaryDataStream ...
type BinaryDataStream struct {
	ID     string `xml:"id,attr" json:"id"`
	Name   string ` xml:"name,attr" json:"name"`
	Size   string `xml:"size" json:"size"`
	Stream string `xml:"Stream,value" json:"stream"`
}

// Calculated ...
type Calculated struct {
	Calculation *Calculation `xml:"Calculation" json:"calculation,omitempty"`
}

// Color ...
type Color struct {
	Red   int     `xml:"red,attr" json:"red"`
	Green int     `xml:"green,attr" json:"green"`
	Blue  int     `xml:"blue,attr" json:"blue"`
	Alpha float64 `xml:"alpha,attr" json:"alpha"`
}

// CoordRect ..
type CoordRect struct {
	Top    int `xml:"top,attr" json:"top"`
	Left   int `xml:"left,attr" json:"left"`
	Bottom int `xml:"bottom,attr" json:"bottom"`
	Right  int `xml:"right,attr" json:"right"`
}

// Defaults ... Defaults used when opening the file
type Defaults struct {
	LayoutReference *LayoutReference `xml:"LayoutReference" json:"layoutReference"`
}

// ExternalValueList ...
type ExternalValueList struct {
	FileReference      *FileReferenceExternal `xml:"FileReference" json:"fileReference"`
	ValueListReference *ValueListReference    `xml:"ValueListReference" json:"valueListReference"`
}

// FieldValueList ...
type FieldValueList struct {
	PrimaryField *PrimaryFieldValueList `xml:"PrimaryField" json:"primaryField,omitempty"`
	ShowRelated  *ShowRelated           `xml:"ShowRelated" json:"showRelated,omitempty"`
}

// FileReferenceExternal ...
type FileReferenceExternal struct {
	ID                int    `xml:"id,attr" json:"id,omitempty"`
	Name              string `xml:"name,attr" json:"name,omitempty"`
	UniversalPathList string `xml:"UniversalPathList" json:"universalPathList,omitempty"`
}

// FileReferenceTableOccurrence ...
type FileReferenceTableOccurrence struct {
	ID        int    `xml:"id,attr" json:"id,omitempty"`
	Name      string `xml:"name,attr" json:"name,omitempty"`
	TableName string `xml:"tableName,attr" json:"tableName,omitempty"`
}

// MetadataAddActionMinimum ... The minimum file stucture that can be used with this definition
type MetadataAddActionMinimum struct {
	Value   int    `xml:"value,attr" json:"value"`
	Version string `xml:"version,attr" json:"version"`
}

// MetadataAddActionHideToolbars ... Are the toolbars hidden on opening the file
type MetadataAddActionHideToolbars struct {
	Enable bool `xml:"enable,attr" json:"enable"`
}

// IconData ... Icon associated with the file.
type IconData struct {
	Scale float64 `xml:"scale,attr" json:"scale"`
	Type  int     `xml:"type,attr" json:"type"`

	Large    *IconDataLarge    `xml:"Large" json:"large"`
	Mini     *IconDataMini     `xml:"Mini" json:"mini"`
	Original *IconDataOriginal `xml:"Original" json:"original"`
	Small    *IconDataSmall    `xml:"Small" json:"small"`
}

// IconDataLarge ...
type IconDataLarge struct {
	*BinaryData `xml:"BinaryData" json:"binaryData"`
}

// IconDataMini ...
type IconDataMini struct {
	*BinaryData `xml:"BinaryData" json:"binaryData"`
}

// IconDataOriginal ...
type IconDataOriginal struct {
	*BinaryData `xml:"BinaryData" json:"binaryData"`
}

// IconDataSmall ...
type IconDataSmall struct {
	*BinaryData `xml:"BinaryData" json:"binaryData"`
}

// LanguageReference name="English" id="21"/> ...
type LanguageReference struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string ` xml:"name,attr" json:"name"`
}

// Login ... Any default login details
type Login struct {
	Type     int    `xml:"type,attr" json:"type"`
	UserName string `xml:"UserName" json:"userName"`
}

// Repetition ...
type Repetition struct {
	Value int `xml:"value,attr" json:"value"`
}

// SerialNumber increment="1" nextvalue="4" generate="OnCreation"/> ...
type SerialNumber struct {
	Generate  string `xml:"generate,attr" json:"generate"`
	Increment int    `xml:"increment,attr" json:"increment"`
	Nextvalue string `xml:"nextvalue,attr" json:"-"`
}

// SourceValueList ...
type SourceValueList struct {
	Value string `xml:"value,attr" json:"value"`
}

// ShowRelated ...
type ShowRelated struct {
	Value                    bool                      `xml:"value,attr" json:"value"`
	TableOccurrenceReference *TableOccurrenceReference `xml:"TableOccurrenceReference" json:"tableOccurrenceReference"`
}

// <ValueListReference id="84" name="COUNTRY-code-name-VALUE-LIST"/>

// Validation ...
type Validation struct {
	AllowOverride bool   ` xml:"AllowOverride,attr" json:"allowOverride"`
	Existing      bool   ` xml:"existing,attr" json:"existing"`
	NotEmpty      bool   ` xml:"notEmpty,attr" json:"notEmpty"`
	Type          string ` xml:"type,attr" json:"type"`
	Unique        bool   ` xml:"unique,attr" json:"unique"`
}

func createBaseDirectoryCatalogDirectories(baseDirectoryCatalog *BaseDirectoryCatalog, rootPath string) error {
	var err error
	var directoryPath string
	var metaPath string
	var baseDirectory *BaseDirectory
	if baseDirectoryCatalog != nil {
		for _, baseDirectory = range baseDirectoryCatalog.BaseDirectorySlice {
			directoryPath = filepath.Join(rootPath, baseDirectory.Name)
			err = createDirectoryIfItDoesNotExist(directoryPath, 0775)
			if err == nil {
				metaPath = ".meta.json"
				err = saveJSONByteSliceToPath(baseDirectory, directoryPath, metaPath)
				if err != nil {
					break
				}
			}
		}
	}
	return err
}

func createBaseTableDirectories(baseTableCatalog *BaseTableCatalog, rootPath string) error {
	var err error
	var directoryPath string
	var metaPath string
	var baseTable *BaseTable
	if baseTableCatalog != nil {
		for _, baseTable = range baseTableCatalog.BaseTableSlice {
			directoryPath = filepath.Join(rootPath, baseTable.Name)
			err = createDirectoryIfItDoesNotExist(directoryPath, 0775)
			if err == nil {
				metaPath = ".meta.json"
				err = saveJSONByteSliceToPath(baseTable, directoryPath, metaPath)
				if err != nil {
					break
				}
			}
		}
	}
	return err
}

func createTableOccurencesDirectories(tableOccurrenceCatalog *TableOccurrenceCatalog, rootPath string) error {
	var err error
	var directoryPath string
	var metaPath string
	var tableOccurrence *TableOccurrence
	if tableOccurrenceCatalog != nil {
		for _, tableOccurrence = range tableOccurrenceCatalog.TableOccurrenceSlice {
			directoryPath = filepath.Join(rootPath, tableOccurrence.Name)
			err = createDirectoryIfItDoesNotExist(directoryPath, 0775)
			if err == nil {
				metaPath = ".meta.json"
				err = saveJSONByteSliceToPath(tableOccurrence, directoryPath, metaPath)
				if err != nil {
					break
				}
			}
		}
	}
	return err
}

// CreateDirectoryIfItDoesNotExist ...
// https://siongui.github.io/2017/03/28/go-create-directory-if-not-exist/
func createDirectoryIfItDoesNotExist(path string, fileMode os.FileMode) error {
	var err error
	if _, err = os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, fileMode)
	}
	return err
}

func getTransformReader(path string) (*transform.Reader, error) {

	var err error
	var file *os.File
	var utf16littleEndian encoding.Encoding
	var utf16ByteOrderMarker transform.Transformer
	var transformReader *transform.Reader

	file, err = os.Open(path)
	if err == nil {
		utf16littleEndian = unicode.UTF16(unicode.LittleEndian, unicode.IgnoreBOM)
		utf16ByteOrderMarker = unicode.BOMOverride(utf16littleEndian.NewDecoder())
		transformReader = transform.NewReader(file, utf16ByteOrderMarker)
	}

	return transformReader, err
}

func saveJSONByteSliceToPath(model interface{}, directory string, fileName string) error {
	var byteSlice []byte
	var err error
	var file *os.File
	var path string
	path = filepath.Join(directory, RemoveInvalidWindowsPathCharacters(fileName))
	byteSlice, err = json.MarshalIndent(model, "", "    ")
	if err == nil {
		fmt.Println(path)
		file, err = os.Create(path)
		if err == nil {
			defer file.Close()
			_, err = file.Write(byteSlice)
			if err == nil {
				err = file.Sync()
			}
		}
	}
	return err
}

func readConfig(dirctoryPath string) ([]string, error) {

	var err error
	var result []string
	var name string
	var fileInfoSlice []os.FileInfo
	var fileInfo os.FileInfo

	fileInfoSlice, err = ioutil.ReadDir(dirctoryPath)
	if err == nil {
		for _, fileInfo = range fileInfoSlice {
			name = fileInfo.Name()
			if path.Ext(name) == ".xml" {
				result = append(result, filepath.Join(dirctoryPath, fileInfo.Name()))
			}
		}
	}

	return result, err
}

func main() {

	var applicationDirectory string
	var environmentPath string
	var fmDynamicTemplate *FMDynamicTemplate
	var byteSlice []byte
	var err error
	var directoryPath = "./"
	var pathSlice []string
	var path string
	var transformReader *transform.Reader
	var xmlDecoder *xml.Decoder
	var last time.Time
	var elapsed time.Duration

	fmt.Println(version())

	last = time.Now()

	applicationDirectory, err = filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	environmentPath = filepath.Join(applicationDirectory, ".env")
	if utilities.PathIsFile(environmentPath) {
		if utilities.PathExists(environmentPath) {
			fmt.Println(environmentPath)
			err = godotenv.Load(environmentPath)
		}
	}
	if err != nil {
		panic(err)
	}

	if len(os.Args) > 1 {
		directoryPath = os.Args[1]
	} else {
		for _, e := range os.Environ() {
			//fmt.Println(e)
			pair := strings.Split(e, "=")
			switch pair[0] {
			case "FILEMAKER_DEFAULT_PROJECT_ROOT":
				directoryPath = pair[1]
			}
		}
	}

	fmt.Printf("directoryPath: %s\n", directoryPath)
	pathSlice, err = readConfig(directoryPath)

	if err != nil {
		panic(err)
	} else {
		for _, path = range pathSlice {

			transformReader, err = getTransformReader(path)
			byteSlice, err = ioutil.ReadAll(transformReader)
			if err != nil {
				panic(err)
			} else {
				fmDynamicTemplate = &FMDynamicTemplate{}
				xmlDecoder = xml.NewDecoder(bytes.NewReader(byteSlice))
				err = xmlDecoder.Decode(&fmDynamicTemplate)
				if err != nil {
					panic(err)
				} else {
					fmt.Printf("%+v\n", fmDynamicTemplate.File)
					err = fmDynamicTemplate.Process(path)
					elapsed = time.Since(last)
					fmt.Printf("took %s\n", elapsed)
					last = time.Now()
					if err != nil {
						fmt.Println(err)
					}
				}
			}
		}
		fmt.Println(version())
	}
}
