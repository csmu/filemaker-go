package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *ScriptCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *Script
	if catalog != nil {
		for _, model = range catalog.ScriptSlice {
			if model.Name != "" {
				metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
				saveJSONByteSliceToPath(model, rootPath, metaPath)
			}
		}
	}
	return err
}

// OptionsScript ...
type OptionsScript struct {
	Access            string `xml:"access,attr" json:"access,omitempty"`
	Compatibility     int    `xml:"compatibility,attr" json:"compatibility,omitempty"`
	Hidden            bool   `xml:"hidden,attr" json:"hidden,omitempty"`
	Runwithfullaccess bool   `xml:"runwithfullaccess,attr" json:"runwithfullaccess,omitempty"`
	Value             int    `xml:",chardata" json:"value,omitempty"`
}

// Script ...
type Script struct {
	ID      int            `xml:"id,attr" json:"id,omitempty"`
	Name    string         `xml:"name,attr" json:"name,omitempty"`
	TagList string         `xml:"TagList" json:"-"`
	Options *OptionsScript `xml:"Options" json:"options,omitempty"`
}

// ScriptCatalog ...
type ScriptCatalog struct {
	Membercount int       `xml:"membercount,attr" json:"memberCount"`
	ScriptSlice []*Script `xml:"Script" json:"script"`
}

// ScriptReference ...
type ScriptReference struct {
	ID   int    `xml:"id,attr" json:"id"`
	Name string `xml:"name,attr" json:"name"`
}

// ScriptTrigger ... a trigger for a script
type ScriptTrigger struct {
	ScriptReference *ScriptReference `xml:"ScriptReference" json:"scriptReference"`
}

// ScriptTriggers ... an list of script triggers
type ScriptTriggers struct {
	Membercount   int              `xml:"membercount,attr" json:"memberCount"`
	ScriptTrigger []*ScriptTrigger `json:"scriptTrigger"`
}
