package filemaker

import (
	"fmt"
)

// BaseTable ... The real table. In SQL terms the 'table'
type BaseTable struct {
	Name    string `xml:"name,attr" json:"name"`
	ID      int    `xml:"id,attr" json:"id"`
	TagList string `xml:"TagList" json:"-"`
}

// BaseTableCatalog ... The lisy fo tables in the Filemaker file
type BaseTableCatalog struct {
	Membercount    int          `xml:"membercount,attr" json:"memberCount"`
	BaseTableSlice []*BaseTable `xml:"BaseTable" json:"baseTable"`
}

// BaseTableReference ...
type BaseTableReference struct {
	Name string `xml:"name,attr" json:"name"`
	ID   int    `xml:"id,attr" json:"id"`
}

// Process ...
func (catalog *BaseTableCatalog) Process(path string) error {
	var err error
	var metaPath string
	var model *BaseTable
	if catalog != nil {
		for _, model = range catalog.BaseTableSlice {
			if model.Name != "" {
				metaPath = fmt.Sprintf("%s (%d).json", model.Name, model.ID)
				saveJSONByteSliceToPath(model, path, metaPath)
			}
		}
	}
	return err
}
