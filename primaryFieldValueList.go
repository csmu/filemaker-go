package filemaker

// PrimaryFieldValueList ...
type PrimaryFieldValueList struct {
	Show           bool            `xml:"show,attr" json:"show"`
	Sort           bool            `xml:"sort,attr" json:"sort"`
	FieldReference *FieldReference `xml:"FieldReference" json:"fieldReference"`
}
