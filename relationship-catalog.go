package filemaker

import (
	"fmt"
)

// Process ...
func (catalog *RelationshipCatalog) Process(rootPath string) error {
	var err error
	var metaPath string
	var model *Relationship
	if catalog != nil {
		for _, model = range catalog.RelationshipSlice {
			if err == nil {
				metaPath = fmt.Sprintf("%s %s (%d).json", model.LeftTable.Name, model.RightTable.Name, model.ID)
				saveJSONByteSliceToPath(model, rootPath, metaPath)
			}
		}
	}
	return err
}

// FieldReferenceJoinPredicate ...
type FieldReferenceJoinPredicate struct {
	ID         int    `xml:"id,attr" json:"id"`
	Name       string `xml:"name,attr" json:"name"`
	Repetition int    `xml:"repetition,attr" json:"repetition"`
	BaseTable  string `xml:"baseTable,attr" json:"baseTable"`
}

// JoinPredicate ...
type JoinPredicate struct {
	Type       string      `xml:"type,attr" json:"type"`
	LeftField  *LeftField  `xml:"LeftField" json:"leftField"`
	RightField *RightField `xml:"RightField" json:"rightField"`
}

// JoinPredicateList ...
type JoinPredicateList struct {
	Membercount   int              `xml:"membercount,attr" json:"memberCount"`
	JoinPredicate []*JoinPredicate `xml:"JoinPredicate" json:"joinPredicate"`
}

// LeftField ...
type LeftField struct {
	FieldReference *FieldReferenceJoinPredicate `xml:"FieldReference" json:"fieldReference"`
}

// LeftTable ...
type LeftTable struct {
	CascadeCreate     bool               `xml:"cascadeCreate,attr" json:"cascadeCreate"`
	CascadeDelete     bool               `xml:"cascadeDelete,attr" json:"cascadeDelete"`
	Name              string             `xml:"name,attr" json:"name"`
	Type              string             `xml:"type,attr" json:"type"`
	SortSpecification *SortSpecification `xml:"SortSpecification" json:"sortSpecification"`
}

// RightField ...
type RightField struct {
	FieldReference *FieldReferenceJoinPredicate `xml:"FieldReference" json:"fieldReference"`
}

// RightTable ...
type RightTable struct {
	CascadeCreate     bool               `xml:"cascadeCreate,attr" json:"cascadeCreate"`
	CascadeDelete     bool               `xml:"cascadeDelete,attr" json:"cascadeDelete"`
	Name              string             `xml:"name,attr" json:"name"`
	Type              string             `xml:"type,attr" json:"type"`
	SortSpecification *SortSpecification `xml:"SortSpecification" json:"sortSpecification"`
}

// Relationship ...
type Relationship struct {
	ID                int                `xml:"id,attr" json:"id"`
	LeftTable         *LeftTable         `xml:"LeftTable" json:"leftTable"`
	RightTable        *RightTable        `xml:"RightTable" json:"rightTable"`
	JoinPredicateList *JoinPredicateList `xml:"JoinPredicateList" json:"joinPredicateList"`
}

// RelationshipCatalog ...
type RelationshipCatalog struct {
	Membercount       int             `xml:"membercount,attr" json:"memberCount"`
	RelationshipSlice []*Relationship `xml:"Relationship" json:"relationship"`
}
